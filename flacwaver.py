#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import getpass
import subprocess
import argparse

def initArgs():
	parser = argparse.ArgumentParser(description="Convert files from flac to wav, \
		or wav to flac with -r flag")
	parser.add_argument("-r", "--reverse", action="store_true", 
		dest="reverse", default=False, help="convert wav to flac instead of flac to wav")
	parser.add_argument("-s", "--source", action="store", 
		dest="indir", default=None, help="specify source directory")
	parser.add_argument("-d", "--dest", action="store", 
		dest="destdir", default=None, help="specify destination directory")
	parser.add_argument("--use-src", action="store_true", 
		dest="usesrc", default=False, help="use source directory as destination directory.")
	parser.add_argument("--rip", action="store_true", 
		dest="rip", default=False, help="Rip tracks in WAV format from a disc")
	parser.add_argument("--convert", action="store_true", 
		dest="rip_convert", default=False, help="Rip tracks in WAV format from a disc, and then convert to FLAC")

	args = parser.parse_args()

	if args.usesrc and args.indir is None:
		print("Must enter a source directory when using --use-src flag.")
		sys.exit(1)

	if args.rip and args.reverse:
		print("Cannot use -r flag with --rip")
		sys.exit(1)

	return args

# Interface

def startup():
	try:
		subprocess.call(["ffmpeg"])
	except OSError as e:
		if e.errno == os.errno.ENOENT:
			print("Unable to locate ffmpeg.  Make sure it's installed and added to PATH.")
			sys.exit(1)
		else:
			print("Unknown error when trying to invoke ffmpeg.")
			sys.exit(1)

def convert():
	src = determineSourceDir()
	dest = determineDestDir()

	for f in os.listdir(src):
		name = f[:f.rfind(".")]

def convertToFlac(src, dest, name):
	if not fullValidate(src, dest, name):
		sys.exit()

	subprocess.call(["ffmpeg", "-i", src+"/"+name+".wav", dest+"/"+name+".flac"])
	print("Generated file: %s in directory %s\n" % (name+".flac", dest))

def convertToWav(src, dest, name):
	if not fullValidate(src, dest, name):
		sys.exit()

	subprocess.call(["ffmpeg", "-i", src+"/"+name+".flac", dest+"/"+name+".wav"])
	print("Generated file: %s in directory %s\n" % (name+".wav", dest))	

def determineSourceDir():
	raise NotImplemented

def determineDestDir():
	raise NotImplemented

def validateFile(path, filename):
	if os.path.isfile(path + "/" + filename):
		return True
	else:
		return False

def validatePath(path):
	if not os.path.exists(path):
		return False
	else:
		if os.path.isdir(path):
			return True
		else:
			return False

def fullValidate(src, dest, filename):
	if not validatePath(src):
		print("Invalid source directory %s, doesn't exist" % src)
		return False
	elif not validatePath(dest):
		print("Invalid destination directory %s, doesn't exist" % dest)
		return False
	elif not validateFile(filename):
		print("Invalid filename %s, doesn't exist" % filename)
		return False
	else:
		return True			

def getPlatform():
	platform = sys.platform
	if platform == "linux":
		return platform
	elif platform == "darwin":
		return platform	
	elif platform == "win32":
		return platform
	else:
		print("Unknown OS.  Program compatible only on Windows, Linux, and OSX.")
		sys.exit()

# Console	

def fixDir(directory):
	if directory[-1:] != "/":
		directory += "/"

	if checkLinuxConv(directory):
		return "C:/" + directory
	else:
		return directory

def checkLinuxConv(directory):
	if directory[0] == "/" and directory[1] == "c" and directory[2] == "/":
		directory = directory[3:]
		return True
	else:
		return False

def enterSourcedir():
	print("Enter source directory")
	print("Preferred Format: C:/Users/%s/Desktop" % getpass.getuser())
	sourcedir = input(">")
	return sourcedir

def enterOutdir():
	print("Enter output directory (press enter to use source directory as output)")
	print("Preferred Format: C:/Users/%s/Desktop" % getpass.getuser())
	outdir = input(">")
	return outdir

def convert_cmdline(args):
	try:
		if not args.indir:
			sourcedir = ''

			while sourcedir == '':
				sourcedir = enterSourcedir()
				if not args.destdir:	
					outdir = enterOutdir()
				print("\n")
				if not os.path.exists(sourcedir):
					sourcedir = ''

		sourcedir = args.indir
		outdir = args.destdir
	except KeyboardInterrupt:
		print("\nDetected Keyboard Interrupt - Exiting")
		sys.exit(0)

	if not outdir:
		print("Using source directory as output diredctory")
		outdir = sourcedir

	fixed_sourcedir = fixDir(sourcedir)

	if not os.path.exists(outdir):
		print("Provided outdir doesn't exist, using sourcedir")
		fixed_outdir = fixed_sourcedir
	else:
		fixed_outdir = fixDir(outdir)

	while os.listdir(sourcedir) == [] or os.listdir(sourcedir) is None:
		print("No audio files found in directory %s" % sourcedir)
		print("Enter another source directory\n")
		sourcedir = enterSourcedir()
		outdir = enterOutdir()

	for f in os.listdir(fixed_sourcedir):
		name = f[:f.rfind(".")]
		print("Calling ffmpeg -i on %s\n" % name+".flac")
		print("Output Directory: %s" % outdir)
		if args.reverse:
			subprocess.call(["ffmpeg", "-i", fixed_sourcedir+"/"+name+".wav", outdir+"/"+name+".flac"])
			print("Generated file: %s in directory %s\n" % (name+".flac", outdir))
		else:
			subprocess.call(["ffmpeg", "-i", fixed_sourcedir+"/"+name+".flac", outdir+"/"+name+".wav"])
			print("Generated file: %s in directory %s\n" % (name+".wav", outdir))	

if __name__ == '__main__':
	args = initArgs()
	startup()
	sys.exit(convert_cmdline(args))